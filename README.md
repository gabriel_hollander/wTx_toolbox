# Approximate polynomial decoupling package

The starting point for the package in the current repository are two coupled multivariate polynomials with real coefficients. 
The MATLAB function ``DecouplePolynomial.m`` can be used to find an approximate decoupled representation of this function, using different kinds of covariance matrices as weighting matrices, as described in my PhD dissertation.
It attempts to find the best possible decoupled representation, according to a cost function defined on the output of the system. 
In this decoupled representation, the output is written as a linear combination of parallel univariate polynomials of linear forms of the input.

## Usage

In this section, the usage of the MATLAB function ``DecouplePolynomial.m`` is discussed in detail.


### Input structure

The function takes the MATLAB structure ``coupledPolynomial`` as argument and outputs the structure ``decoupledPolynomial``. The argument structure should contain the following fields

* ``coupledCoeffs``, the coefficients of the coupled multivariate polynomial,
* ``covarianceMatrix``, the covariance matrix of coupledCoeffs, used in the decoupling method,
* ``CPDtype`` ('no’, 'diag’, 'blockdiag’ or 'full’), type of weighted decoupling,
* ``r``, the number of branches of the decoupling.
* ``lambda``, a metaparameter to be used in the case the covarance matrix has low rank. If not given, the default value is 1.

In the case no covariance matrix or an empty covariance matrix is given, then the implementation performs an unweighted decoupling. 
This implementation performs similarly to and generalizes other implementations like [Tensorlab](https://www.tensorlab.net/) or the [Nway toolbox](https://nl.mathworks.com/matlabcentral/fileexchange/1088-the-n-way-toolbox?requestedDomain=true).

### Output structure

The output of the function is the MATLAB structure ``decoupledPolynomial`` containing all the information about the decoupled polynomial. It contains the following fields:

* ``type``, the type of the decoupling,
* ``We``, the transformation matrix W ('e’ stands for 'estimated’),
* ``Ve``, the transformation matrix V ('e’ stands for 'estimated’),
* ``Ge``, the coefficients of the decoupled polynomials,
* ``iteration_count``, the number of iterations before the end of the algorithm,
* ``relerr``, a measure for the relative error of the decoupling.


## Example code and output

This part shows a small example using this implementation. We define the coefficients of the coupled polynomial function with the statement

``coupledPolynomial.approximatedCoeffs = …``

These coefficients are organized in columns, one for every output of the function, and are ordered using the list of monomials (for 2 inputs and degree 3)

``1, u1, u2, u1^2, u1u2, u2^2, u1^3, u1^2u2, u1*u2^2, u2^3``.

Next, the covariance matrix is defined by the statement

``coupledPolynomial.covarianceMatrix = …``

Also the number of branches of the decoupled representation is defined by

``coupledPolynomial.r = …``

and the type of weighted decoupled is given by ``coupledPolynomial.CPDtype = …;``

In the case the covariance matrix is rank-deficient, the hyperparameter ``lambda`` can be defined (it is set to 1 as a default value) by

``coupledPolynomial.lambda = …;``

Finally, the decoupling process is performed with the command

``decoupledPolynomial = DecouplePolynomial(coupledPolynomial);``

The output of this function is the structure ``decoupledPolynomial`` with the following fields:

* ``type``: decoupling with full weight
* ``We``: 2x2 double
* ``Ve``: 2x2 double
* ``Ge``: 2x4 double
* ``iteration_count``: 20
* ``relerr``: 0.0096

The matrix ``Ge`` contains the coefficients of the decoupled polynomials, ordered from high to low degree, one row per polynomial.

## Authors
This work has been developed by Gabriel Hollander under the supervision of Philippe Dreesen, Mariya Ishteva, Ivan Markovsky and Johan Schoukens.

## License
Permission is granted to freely use the code in this package for non-commercial applications only. The code is provided ‘‘as is’’ without any warranty. The rest of this section presents the full license.

**Full license**

Decoupling Polynomial: Software License Agreement

Copyright (c) 2016, Vrije Universiteit Brussel - dept. ELEC. All rights reserved.

This software, Decoupling Polynomial (the “Program”), is being licensed.

Conditions (liability, etc.) are listed below.

**General terms**

Licence grant. Vrije Universiteit Brussel - dept. ELEC grants to licensee a nonexclusive license to freely use the Program for non-commercial applications only.

**Conditions**

*Limited warranty/limitation of remedies.*

Except as expressly provided by this agreement (or as implied by law where the law provides that the particular terms implied cannot be excluded by contract), all other conditions, warranties, or other terms (including any with regard to infringement, merchantable quality, or fitness for purpose) are excluded. 
Some states do not allow limitations on how long an implied warranty lasts, so the above limitation may not apply to licensee. this warranty gives licensee specific legal rights and licensee may also have other rights which vary from state to state. 
Licensee accepts responsibility for its use of the program and the results obtained therefrom.

*Limitation of liability.*

The program should not be relied on as the sole basis to solve a problem whose incorrect solution could result in injury to person or property. 
If a program is employed in such a manner, it is at the licensee's own risk and VUB explicitly disclaims all liability for such misuse to the extent allowed by law. 
VUB's liability for death or personal injury resulting from negligence or for any other matter in relation to which liability by law cannot be excluded or limited shall not be excluded or limited. 
Except as aforesaid, VUB shall have no liability for any indirect or consequential loss (whether foreseeable or otherwise and including loss of profits, loss of business, loss of opportunity, and loss of use of any computer hardware or software). 
Some states do not allow the exclusion or limitation of incidental or consequential damages, so the above exclusion or limitation may not apply to licensee.
